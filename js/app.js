(function() {
    var app = angular.module('Trebon-Viewer', []);

    app.service('tabPosition', function() {
        var tab = 0;

        this.setTab = function(newtab) {
            this.tab = newtab;
        };

        this.getTab = function() {
            return this.tab;
        };
    });

    app.controller("ViewerCtrl", ['$http', '$timeout', 'tabPosition', function($http, $timeout, tabPosition) {
        var that = this;
        this.participantes = [];
        this.urlBase = "proxy/index.php?ipRequest=http://www.vola-racing.com/rallypro/fedegalegaauto/text.php?file=data%id%_%n%.txt";//"http://www.vola-racing.com/rallypro/fedegalegaauto/text.php?file=data%id%_%n%.txt";//
        this.urlRista = "proxy/index.php?ipRequest=http://www.vola-racing.com/rallypro/fedegalegaauto/live.php";//"http://www.vola-racing.com/rallypro/fedegalegaauto/live.php"//
        //this.urlBase = "http://www.vola-racing.com/rallypro/fedegalegaauto/text.php?file=data%id%_%n%.txt";//"http://www.vola-racing.com/rallypro/fedegalegaauto/text.php?file=data%id%_%n%.txt";//
        //this.urlRista = "http://www.vola-racing.com/rallypro/fedegalegaauto/live.php";//"http://www.vola-racing.com/rallypro/fedegalegaauto/live.php"//
        this.posArrayParam = "&posArray=";
        this.ristaDatos = "112;475;131;506;333;91;;;;;563;223;813;121;591;";
        this.positionPenalitie = -1;
        this.positionDNF = -1;
        this.elementsToWait=-1;

        
        this.menu = [];

        this.loadAllThings = false;




        this.endLoad = function(){
            if(!that.loadAllThings){
                //Lo guardo en una variable para evitar tener que comprobar todo esto muchas veces, entonces cuando ya se cargó todo ya está
                if(that.informationRallye !== undefined && this.participantes.length===this.elementsToWait && that.informationRallye.info.groups.Penalties.list !== undefined && that.informationRallye.info.groups.DNF.list!==undefined){
                //if(that.informationRallye !== undefined){
                        tabPosition.setTab(that.lastTramo());
                        that.loadAllThings = true;
                }
                
            }
            return that.loadAllThings;
        };

        this.startFunctionToObtainMenu = function(){
            $http.get(that.urlBase.replace("%id%",1).replace("%n%",that.getSubId(that.ristaDatos,1,";"))+"&r=" + Math.random() % 1e4).success(function(data) {
                    that.informationRallye = data;//Parser.parseInformationRallye(data);
                    //console.log(that.informationRallye);
                    var elements = (that.informationRallye.tramos.length*2)-1;
                    that.positionPenalitie = elements;
                    that.positionDNF = elements+1;
                    that.menu = new Array(elements);
                    that.participantes = new Array(elements);
                    that.startFunctionToObtainData();
                    that.elementsToWait = ((that.informationRallye.tramos.length*2)-1);
                    
            }).error(function(data, status, headers, config) {
                    console.log("Not found menu in: "+config.url);
                
            });
        };

        this.startApp = function(){
            $http.get(that.urlRista+"?r=" + Math.random() % 1e4).success(function(data) {
                    that.ristaDatos = data;
                    that.startFunctionToObtainMenu();
            }).error(function(data, status, headers, config) {
                    console.log("Imposible obtain initial data: "+config.url);
                
            });
        };
        
        this.getSubId = function(ristaResponse, idPos, spliter) {
            var d = "";
            var e = 1;
            for (pos = 0; pos < ristaResponse.length; pos++) {
                if (ristaResponse.charAt(pos) == spliter) {
                    if (e == idPos) return d%10;
                    e = e + 1
                } else if (e == idPos) {
                    d = d + ristaResponse.substr(pos, 1)
                }
            }
            return d;
        };

        this.obtainDataTramos = function(){
            for(var tramo=0;tramo<that.informationRallye.tramos.length;tramo++){
                    var urlCall = this.generateURlSinceIdRequest(that.informationRallye.tramos[tramo].start, tramo);
                    
                    //console.log(urlCall);
                    $http.get(urlCall).success(function(data, status, headers, config) {
                        if(data==""){
                            console.log("Not found data in: "+config.url);
                            that.elementsToWait -=1;
                        }else{
                            var paramsUrl = config.url.split("=");
                            var arrayPosition =  parseInt(paramsUrl[paramsUrl.length-1]);
                            var informacion = data;//Parser.initParserTramo(data);
  
                            var pos_in_array_menu = arrayPosition*2;                          
                            
                            if(informacion.partial===undefined){//En el caso de el final no existe parcial que es lo que pintamos. Asiq temporalmente mientras no arreglamos como pitnamos los generales finales va asi
                                that.menu[pos_in_array_menu] = that.informationRallye.tramos[arrayPosition].code;
                                that.participantes[pos_in_array_menu] = informacion.final;
                            }else{
                                that.menu[pos_in_array_menu] = that.informationRallye.tramos[arrayPosition].code;
                                that.participantes[pos_in_array_menu] = informacion.partial;
                                that.menu[pos_in_array_menu+1] = that.informationRallye.tramos[arrayPosition].code+" Final";
                                that.participantes[pos_in_array_menu+1] = informacion.final;
                            }

                            //that.participantes[arrayPosition] = informacion;
                            //console.log(that.participantes);
                        }
                    }).error(function(data, status, headers, config) {
                            console.log("Not found data in: "+config.url);
                    
                    });                                    
                    
                }
        },

        this.obtainDataPenalities = function(){
                    var urlCall = this.generateURlSinceIdRequest(that.informationRallye.info.groups.Penalties.start, that.positionPenalitie);
                    //console.log(urlCall);
                    $http.get(urlCall).success(function(data, status, headers, config) {
                        if(data==""){
                            console.log("Not found data in: "+config.url);
                            that.informationRallye.info.groups.Penalties.list = [];
                        }else{
                            var paramsUrl = config.url.split("=");
                            var arrayPosition =  parseInt(paramsUrl[paramsUrl.length-1]);
                            that.informationRallye.info.groups.Penalties.list = data;//Parser.parsePenalityData(data);
                            
                            that.menu[arrayPosition] = that.informationRallye.info.groups.Penalties.code;

                            //Lo metemos para leerlo
                        }
                    }).error(function(data, status, headers, config) {
                            console.log("Not found data in: "+config.url);
                    
                    });                                    
                    
        },

        this.obtainDataDNF = function(){
                    var urlCall = this.generateURlSinceIdRequest(that.informationRallye.info.groups.DNF.start, that.positionDNF);
                    //console.log(urlCall);
                    $http.get(urlCall).success(function(data, status, headers, config) {
                        if(data==""){
                            console.log("Not found data in: "+config.url);
                            that.informationRallye.info.groups.DNF.list = [];
                        }else{
                            var paramsUrl = config.url.split("=");
                            var arrayPosition =  parseInt(paramsUrl[paramsUrl.length-1]);
                            that.informationRallye.info.groups.DNF.list = data;//Parser.parseDNFData(data);
                            
                            that.menu[arrayPosition] = that.informationRallye.info.groups.DNF.code;

                            //Lo metemos para leerlo
                        }
                    }).error(function(data, status, headers, config) {
                            console.log("Not found data in: "+config.url);
                    
                    });                                    
                    
        },

        this.generateURlSinceIdRequest =  function(id, positionResponse){
            return that.urlBase.replace("%id%",id).replace("%n%",that.getSubId(that.ristaDatos,id,";"))+"&r="+ Math.random() % 1e4 +that.posArrayParam+positionResponse;
        },

        this.lastTramo =  function(){
            var last = 0;
            for(var pos = 0;pos<this.participantes.length;pos++){
                try{
                    if(this.participantes[pos].length>0){
                        last = pos;
                    }
                }catch(ex){
                    //No llegaron aún los datos de este tramos
                }
                
            }
            if(last>0){
                last-=1;//para que no sea el final sino el anterior
            }

            return last;
            
        },

        this.startFunctionToObtainData = function(){
            that.obtainDataTramos();
            that.obtainDataPenalities();
            that.obtainDataDNF();


            $timeout(function() {
                  //$scope.getData();
                that.startFunctionToObtainData();
            }, 120000)
        };
        
        // Kick off the interval
        this.startApp();
    }]);


    
    
    app.controller("PanelController", function(tabPosition){


        this.selectTab = function(setTab) {
            tabPosition.setTab(setTab);
        },

        this.getSelectedTab = function(){
            return tabPosition.getTab();
        },
        
        this.isSelected = function(checkTab){
            return tabPosition.getTab()===checkTab;
        },

        this.tagNameCup = function(cup){
            if(cup!==undefined){
                return cup.replace( /[^-A-Za-z0-9]+/g, '_' ).toLowerCase();
            }else{
                return "";
            }
            
        },

        this.getBackgroundColour = function(cup1, cup2, cup3) {
            cup1 = this.tagNameCup(cup1);
            cup2 = this.tagNameCup(cup2);
            cup3 = this.tagNameCup(cup3);
            if(cup1==="top_ten_pirelli" || cup2 === "top_ten_pirelli" || cup3 === "top_ten_pirelli"){
                return this.colorsTag["top_ten_pirelli"];
            }else if(cup1==="volante_fga" || cup2 === "volante_fga" || cup3 === "volante_fga"){
                return this.colorsTag["volante_fga"];
            }else if(cup1==="pirelli_amf" || cup2 === "pirelli_amf" || cup3 === "pirelli_amf"){
                return this.colorsTag["pirelli_amf"];
            }else if(cup1==="seguro_x_d_as" || cup2 === "seguro_x_d_as" || cup3 === "seguro_x_d_as"){
                return this.colorsTag["seguro_x_d_as"];
            }else if(cup1==="galfer" || cup2 === "galfer" || cup3 === "galfer"){
                return this.colorsTag["galfer"];
            }


            /*Pirelli
            Volante FGA
            Pirelli Base
            star box (que e a dos marbella)
            eguro X dias
            Copa galfer*/
            return "default-tag";

        },

        /*this.colorsTag = {
            top_ten_pirelli: "label top_ten_pirelli",
            elf: "label elf",
            volante_fga: "label volante_fga",
            galfer: "label galfer",
            volante_fga: "label volante_fga",
            drive_dmack: "label drive_dmack",
            seguro_x_d_as: "label seguro_x_d_as",
        }*/

        this.colorsTag = {
            top_ten_pirelli: "top_ten_pirelli",
            elf: "elf",
            volante_fga: "volante_fga",
            galfer: "galfer",
            pirelli_amf: "pirelli_amf",
            drive_dmack: "drive_dmack",
            seguro_x_d_as: "seguro_x_d_as",
        }


        
    });

})();