var TrebonCookie = {
    COOKIE_AVISO_COOKIE: "cookiesAcepted",
    COOKIE_AUTO_COOKIE: "automaticAcepted",
    ACEPTADO_AVISO_COOKIE: "yes",
    ACEPTADO_AUTO_COOKIE: "yes",

    setCookie: function(cname, cvalue, exdays) {
        var d = new Date();
        d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
        var expires = "expires=" + d.toGMTString();
        document.cookie = cname + "=" + cvalue + "; " + expires;
    },

    getCookie: function(cname) {
        var name = cname + "=";
        var ca = document.cookie.split(';');
        for (var i = 0; i < ca.length; i++)
        {
            var c = ca[i].trim();
            if (c.indexOf(name) == 0)
                return c.substring(name.length, c.length);
        }
        return "";
    },

    aceptCookiesWarning: function(){
        TrebonCookie.setCookie(TrebonCookie.COOKIE_AVISO_COOKIE, TrebonCookie.ACEPTADO_AVISO_COOKIE, 100);
        TrebonCookie.hideCookiesWarning();
    },

    aceptAutomatic: function(){
        TrebonCookie.setCookie(TrebonCookie.COOKIE_AUTO_COOKIE, TrebonCookie.ACEPTADO_AUTO_COOKIE, 100);
        TrebonCookie.hideCookiesWarning();
    },

    checkCookisAcepted: function(){
        if (TrebonCookie.getCookie(TrebonCookie.COOKIE_AVISO_COOKIE) === "") {

            TrebonCookie.visibleCookiesWarning();
            return false;
        } else {
            //Eliminamos el aviso de cookies
            TrebonCookie.hideCookiesWarning();
            //$("#cookiesWarning").remove();
            return true;
        }
    },

    checkAutomaticAcepted: function(){
        if (TrebonCookie.getCookie(TrebonCookie.COOKIE_AUTO_COOKIE) === "") {
            TrebonCookie.visibleCookiesWarning();
            return false;
        } else {
            //Eliminamos el aviso de cookies
            TrebonCookie.hideCookiesWarning();
            //$("#cookiesWarning").remove();
            return true;
        }
    },

    visibleCookiesWarning: function(){
        $("#CookiesWarnning").show();
    },

    hideCookiesWarning: function(){
        $("#CookiesWarnning").hide();
        TrebonCookie.analytics();
    },

    analytics: function(){
        
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-62008744-1', 'auto');
        ga('send', 'pageview');
        return true;

    },

    initial_cookies: function(){
        if(TrebonCookie.checkCookisAcepted() || TrebonCookie.checkAutomaticAcepted()){
            TrebonCookie.hideCookiesWarning();
        }else{
            TrebonCookie.visibleCookiesWarning();
        }
    }

}






