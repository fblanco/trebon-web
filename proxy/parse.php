<?php
	header('Content-Type: text/html; charset=UTF-8');
	define('CARS_REAL_OCURRENCES', 1);


	/*METODOS PARA OBTENER INFORMACION DE TRAMOS*/
	
	function initParserTramo($data) {
            $html2 = preg_replace('/\n/i',"",$data);
             
            $partes = preg_split('/<table><tr class=\"grid_title\">/',$html2);

            $participantes = [];
            if(count($partes)==2){
            	$participantes['final']=getParticipantes($partes[1]);
            }else{
            	$participantes['final']=getParticipantes($partes[2]);
            	$participantes['partial']=getParticipantes($partes[1]);
            }
         
            return $participantes;
    };

        
    function limpiarParticipante($participante){
    	
    
        //Eliminamos lineas innecesarias
            $participante = preg_replace('/<td class=\"fs\">&nbsp;<\/td>/i', "<td class=\"fs\">none</td>",$participante);
            $participante = preg_replace('/<td class=\"(ac rd|fs ac|ar cr)\">&nbsp;<\/td>/i', "",$participante);
            $participante = preg_replace('/<td><\/tr><\/table>/i', "",$participante);
            $participante = preg_replace('/<\/tr><tr class=\"tr[1-2]\">/i', "",$participante);
            $participante = preg_replace('/<td width=\"50%\" valign=\"top\">/i', "</tr>",$participante);


           //Posicion actual
            $participante = "{\"Posicion\": \"" . $participante;


            //Numero del coche en este rallye
            $participante = preg_replace('/<td class=\"ac no\">/i', "\"Numero\": \"",$participante);
            //El piloto
            $participante = preg_replace('/<td class=\"(|cg|cr)\">/', "\"Piloto\": \"",$participante);
            //El copiloto
            $participante = preg_replace('/<td class=\"\">/i', "\"Copiloto\": \"",$participante);
            //Escudería
            $participante = preg_replace('/<td class=\"fs\">/', "\"Escuderia\": \"",$participante);
            //Copa
            $participante = preg_replace('/<td class=\"fs\">/', "\"Copa\": \"",$participante);
            
            //Eliminamos porquería innecesaria no la eliminamos antes xq es necesaria para delimitar la escuderia y la copa
            $participante = preg_replace('/<td class=\"fs\">none<\/td>/i',"",$participante);
            
            
            //Quitamos cousa rara que non sei que é ata que lle pregunte a KTX
            $participante = preg_replace('/<\/td><td class=\"r1 bn\">/i', " - ",$participante);
            $participante = preg_replace('/<\/td><td class=\"rn bn\">/i', " - ",$participante);
            
            //Coche
            $participante = preg_replace('/<td class=\"fs\">/i', "\"Coche\": \"",$participante);
            


            //Tiempo
            $participante = preg_replace('/<td class=\"ar( .{0,2} | )ti\">/i', "\"Tiempo\": \"",$participante);
            //Gap
            $participante = preg_replace('/<td class=\"ar\">/', "\"Gap\": \"",$participante);
            //Interval
            $participante = preg_replace('/<td class=\"ar\">/i', "\"Interval\": \"",$participante);
            
            
            //Grupo
            $participante = preg_replace('/<td><table><tr><td width=\"65%\" class=\"ac bn\">/', "\"Grupo\": \"",$participante);
            //CI
            $participante = preg_replace('/<td><table><tr><td width=\"65%\" class=\"ac bn\">/i', "\"CI\": \"",$participante);
            //Separadores necesarios
            
            $participante = preg_replace('/&nbsp;<\/td>/i', "\",",$participante);
            $participante = preg_replace('/<\/td>/i', "\",",$participante);
            $participante = preg_replace('/,<\/tr>/i', "}",$participante);
            $participante = preg_replace('/, <\/tr>/i', "}",$participante);
    
            $participante = preg_replace('/}<\/table>"/i',"",$participante);
            $participante = preg_replace('/, \"Coche\": \"No \+ Pinchazos\"/i',"",$participante);
            $participante = preg_replace('/<td class=\"ar( .{0,2} | )cr\">/i',"\"Tiempo2\": \"",$participante);


            $participante = preg_replace('/<\/table>/i',"",$participante);


            //Borramos la ELF temporalmente para que ponga en su sitio el coche
            $participante = preg_replace('/"Coche": "ELF",/i',"###",$participante);
            
            $participante = preg_replace('/"(Coche|Copa)": "(|none)",/i',"",$participante);
            
            
            //Esta parte cambia el nombre de los coches si hay varios ya que la parte previa parsea a coches cuando hay muchas copas
            $numberCars = countNumberOcurrences($participante,"Coche");

            if($numberCars===0){
                $participante = preg_replace('/Copa/i', "Coche",$participante);
            }else{
                for($necesarios = CARS_REAL_OCURRENCES;$necesarios<$numberCars;$necesarios++){
                    $participante = preg_replace('/Coche/', "Copa".$necesarios,$participante);
                }
                
                //Volvemos a meter la copa ELF en su sitio
                $participante = preg_replace('/###/i',"\"Copa".$numberCars."\": \"ELF\",",$participante);

            }
        

            //Cosas que sobran
            $participante = preg_replace('/<[^>]*>=",/i',"",$participante);
            $participante = preg_replace('/<td class="(cg|cr)">/i',"Copiloto:\"",$participante);
            $participante = preg_replace('/<td class="[^"]*">(\+|-|)[0-9]+",/i',"",$participante);
            //Si un $participante tiene comillas en el nombre
            $participante = preg_replace('/"([^":]*)""/i', "'$1'\"",$participante);
        
        
        
        	return json_decode($participante);
           /* try{
                return eval('('+$participante+')');
            }catch(ex){
                console.log($participante);
            }*/
                        
	};

    function countNumberOcurrences($string, $subString){

        $aparecences = preg_split('/'.$subString.'/',$string);
        return count($aparecences)-1;
    };

    function getParticipantes ($strParticipantes){
        $participantes = [];
        //Trabajamos ahora sobre el resultado para obtener la informacion
        $strParticipantes = preg_replace('/\r/i', "",$strParticipantes);
        $strParticipantes = preg_replace('/\n/i', "", $strParticipantes);
        //var salida = $strParticipantes.split("<tr class=\"tr[0-9]\"> <td class=\"r([0-9]||n)\">");
        $salida = preg_split('/<tr class=\"tr[0-9]\"> <td class=\"r([0-9]||n)\">/',$strParticipantes);
        
        //Así estraemos por coche la primera entrada no nos servirá porque tiene la información innecesaria para nosotros
        $numberSalidas = count($salida);
        for ($posCoche = 2; $posCoche < $numberSalidas; $posCoche=$posCoche+2) {
            //console.log(Parser.limpiarParticipante(salida[posCoche]));
            //var coche = new Object();
            /*coche.setStringInformacion(Participation.limpiarParticipante(salida[posCoche]));
            coche.setDorsal(Participation.getDorsal(coche.getInformacion().getStringInformation()));
            coche.setRallye(rallye);
            coche.setTramo(tramo);*/
            $participante = limpiarParticipante($salida[$posCoche]);
            array_push($participantes, $participante);
        }
        return $participantes;
	};

	/* FIN DE METODOS PARA OBTENER INFORMACION DE TRAMOS */


	/* METODOS PARA OBTENER INFORMACION DE DNFS */


    function parseDNFdata($data){
        $datos = preg_replace('/\n/i',"",$data);
        $datos = preg_replace('/&nbsp;/i',"",$data);
        $datos = preg_split('/<tr class=\"[^\"]*\">/',$data);

        $devolver = [];
        $lementsArray = count($datos);
        for($i=2;$i<$lementsArray;$i++){
            $item = $datos[$i];
            $item = preg_replace('/<td class=\"[^\"]*\">([^<]*)<\/td>/', "\"numero\": \"$1\",", $item, 1);
            $item = preg_replace('/<td class=\"[^\"]*\">([^<]*)<\/td>/', "", $item, 1);
            $item = preg_replace('/<td class=\"[^\"]*\">([^<]*)<\/td>/', "\"piloto\": \"$1\",", $item, 1);
            $item = preg_replace('/<td class=\"[^\"]*\">([^<]*)<\/td>/', "\"navegante\": \"$1\",", $item, 1);
            $item = preg_replace('/<td class=\"[^\"]*\">([^<]*)<\/td>/', "\"escuderia\": \"$1\",", $item, 1);
            $item = preg_replace('/<td class=\"[^\"]*\">([^<]*)<\/td>/', "\"vehiculo\": \"$1\",", $item, 1);
            $item = preg_replace('/<td class=\"[^\"]*\">([^<]*)<\/td>/', "\"copa\": \"$1\",", $item, 1);
            $item = preg_replace('/<td class=\"[^\"]*\">([^<]*)<\/td>/', "", $item, 3);
            $item = preg_replace('/<td class=\"[^\"]*\">([^<]*)<\/td>/', "\"grupo\": \"$1\",", $item, 1);
            $item = preg_replace('/<td class=\"[^\"]*\">([^<]*)<\/td>/', "\"clase\": \"$1\",", $item, 1);
            $item = preg_replace('/<td class=\"[^\"]*\">([^<]*)<\/td>/', "\"posicion\": \"$1\",", $item, 1);
            $item = preg_replace('/<td class=\"[^\"]*\">([^<]*)<\/td>/', "\"razon\": \"$1\"", $item, 1);
            $item = preg_replace('/<[^\>]*>/i',"", $item);

            //Si un participante tiene comillas en el nombre
            $item = preg_replace('/"([^":]*)""/i', "'$1'\"",$item);
            $item = "{".rtrim($item, "(\n| |,)")."}";

            array_push($devolver, json_decode($item));


        }
        return $devolver;
	};

	/* FIN METODOS PARA OBTENER INFORMACION DE DNFS */

	/* METODOS PARA OBTENER INFORMACION DE PENALITIES */

	function parsePenalityData($data){
        $datos = preg_replace('/\n/i',"",$data);
        $datos = preg_replace('/&nbsp;/i',"",$data);
        $datos = preg_split('/<tr class=\"[^\"]*\">/',$data);

        $devolver = [];
        $lementsArray = count($datos);
        for($i=2;$i<$lementsArray;$i++){
            $item = $datos[$i];
            $item = preg_replace('/<td class=\"[^\"]*\">([^<]*)<\/td>/', "\"numero\": \"$1\",", $item, 1);
            $item = preg_replace('/<td class=\"[^\"]*\">([^<]*)<\/td>/', "", $item, 1);
            $item = preg_replace('/<td class=\"[^\"]*\">([^<]*)<\/td>/', "\"piloto\": \"$1\",", $item, 1);
            $item = preg_replace('/<td class=\"[^\"]*\">([^<]*)<\/td>/', "\"navegante\": \"$1\",", $item, 1);
            $item = preg_replace('/<td class=\"[^\"]*\">([^<]*)<\/td>/', "\"escuderia\": \"$1\",", $item, 1);
            $item = preg_replace('/<td class=\"[^\"]*\">([^<]*)<\/td>/', "\"vehiculo\": \"$1\",", $item, 1);
            $item = preg_replace('/<td class=\"[^\"]*\">([^<]*)<\/td>/', "\"copa\": \"$1\",", $item, 1);
            $item = preg_replace('/<td class=\"[^\"]*\">([^<]*)<\/td>/', "", $item, 3);
            $item = preg_replace('/<td class=\"[^\"]*\">([^<]*)<\/td>/', "\"grupo\": \"$1\",", $item, 1);
            $item = preg_replace('/<td class=\"[^\"]*\">([^<]*)<\/td>/', "\"clase\": \"$1\",", $item, 1);
            $item = preg_replace('/<td class=\"[^\"]*\">([^<]*)<\/td>/', "\"posicion\": \"$1\",", $item, 1);
            $item = preg_replace('/<td class=\"[^\"]*\">([^<]*)<\/td>/', "\"penalizacion\": \"$1\",", $item, 1);
            $item = preg_replace('/<td class=\"[^\"]*\">([^<]*)<\/td>/', "\"razon\": \"$1\"", $item, 1);
            $item = preg_replace('/<[^\>]*>/i',"", $item);

            //Si un participante tiene comillas en el nombre
            $item = preg_replace('/"([^":]*)""/i', "'$1'\"",$item);
            $item = "{".rtrim($item, "(\n| |,)")."}";

            array_push($devolver, json_decode($item));


        }
        return $devolver;
    };

    /* METODOS PARA OBTENER INFORMACION DE PENALITIES */

    /* METODO PARA OBTENER LA INFORMAICON DEL RALI*/
    function parseInformationRallye($data) {
        $html2=str_replace("\n","",$data);


        $html2=preg_replace('/(\'|")menu_(on|off)(\'|")/i',"",$html2);

        
        $html2=preg_replace('/<\/td>/i',",",$html2);
        $html2=preg_replace('/id=\"b([0-9]*)\" class= onclick=\"start\([0-9]*\)[^>]*>([^,]*)/i',"{start:$1, code: \"$2\"}",$html2);
        $html2=preg_replace('/<td id="b([0-9]*)" class= >([^,]*)/i',"{start:$1, code: \"$2\"}",$html2);



        //aqui cambiamos los nombres q los cambian a veces
        $html2=preg_replace('/Abandons/i',"DNF",$html2);
        $html2=preg_replace('/Engagés/i',"Engages",$html2);
        $html2=preg_replace('/Itinéraire/i',"Itineraire",$html2);
        $html2=preg_replace('/Pénalités/i',"",$html2);



        $tramos=preg_replace('/(<[^{]*|&nbsp;)/i',"",$html2);
        $tramos=preg_replace('/(\\w+|\\d+)/i', "\"$1\"", $tramos);
        $tramos=preg_replace('/""/', "\"", $tramos);
        $tramos=preg_replace('/" "/', " ", $tramos);




        $informacion=preg_replace('/<\/tr<\/table><table><tr><td><table><tr>.*/i',"",$html2);
        $informacion=preg_replace('/<[^>]*>/i',"",$informacion);


        $otra_informacion_tramo=preg_split('/<td><table><tr>/',$html2)[2];
        $otra_informacion_tramo=preg_replace('/id=\"b([0-9]*)\"[^\"]*\"[^\"]*\" onclick=\"start\([0-9]*\)[^>]*><a>([^\<]*)<\/a>/i',"\"$2\":{start:$1, code: \"$2\"}",$otra_informacion_tramo);
        $otra_informacion_tramo=preg_replace('/<[^\"]* /i',"",$otra_informacion_tramo);
        $otra_informacion_tramo=preg_replace('/<[^>]*>/i', "", $otra_informacion_tramo);
        $otra_informacion_tramo=preg_replace('/,[^aA-zA0-9]*,/i', "", $otra_informacion_tramo);
        $otra_informacion_tramo=preg_replace('/(\\w+|\\d+)/i', "\"$1\"", $otra_informacion_tramo);
        $otra_informacion_tramo=preg_replace('/""/', "\"", $otra_informacion_tramo);
        $otra_informacion_tramo = "{".$otra_informacion_tramo."}";
        //$otra_informacion_tramo=preg_replace('/" "/', " ", $otra_informacion_tramo);



                //Falta terminar esta parte que es la que se encarga de crear el objeto
        $devolucion = [];
        $info = preg_split('/,/',$informacion);


        $devolucion["info"]["groups"] = json_decode($otra_informacion_tramo);
        $devolucion["info"]["nombreRallye"] = $info[0];
        $devolucion["info"]["campeonatoRallye"] = $info[0];
        $devolucion["info"]["fechaRallye"] = $info[2];



        
        $tramos= preg_replace('/\},[^aA-zZ0-9]*\]/i', "}]", "[".$tramos."]");

        $devolucion["tramos"] = json_decode($tramos);



        return $devolucion;
        
        
    };

    /* FIN METODO PARA OBTENER LA INFORMAICON DEL RALI*/

    function parseData($id, $data){
    	if($id==1){
    		return parseInformationRallye($data);
    	}else if($id==4){
    		return parseDNFdata($data);
    	}else if($id==5){
    		return parsePenalityData($data);
    	}else{
    		return initParserTramo($data);
    	}
    }

?>