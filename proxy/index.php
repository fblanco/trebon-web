<?php
header('Content-Type: text/html; charset=UTF-8');
require_once 'Unirest.php';
require_once 'parse.php';
define('MINUTES_OF_LIFE_OF_THE_CACHE', 5);
define('CACHE_LOCATION', 'cache/');
define('CACHE_EXTENSION', '.txt');
 
 
function proxy(){
    try{
        if(isset($_GET["ipRequest"])){
            echo cache($_GET["ipRequest"]);
        }else{
            echo "NOT FOUND URL REQUEST";
        }
    }catch (Exception $e) {
        echo $e->errorMessage();
        http_response_code(500);
    }
     
 
}
 
 
function cache($url){
    $cache_file = file_name($url); // Create a unique name for the cache file using a quick md5 hash.
 
    $file = null;
 
    if (!file_exists($cache_file)){
        $response = request($url);//file_get_contents($url);// // Fetch the file.
        if(!isset($response)){
            $cache_file = get_last_file($url);
        }else{
            $file = $response;
            save_cache($url, $file);
        }
    }
    if(!isset($file)){
        $file = file_get_contents($cache_file); // Get the file from the cache.
    }
     
    return $file;
}
 
 
function save_cache($url, $file){
    $path = path_cache($url);
 
    if(!file_exists($path)){
        if(!mkdir($path, 0777, true)){
            throw new Exception("Imposible create directory");
        }
 
    }
 
    $cache_file = file_name($url);
 
    file_put_contents($cache_file, $file, LOCK_EX);
}
 
 
function path_cache($url){
    $carpeta = sha1($url);
    return CACHE_LOCATION.$carpeta."/";
}
 
 
function file_name($url){
    $actual_moment = split(":", date('i:Y_m_d_H_'));
    $actual_minute = $actual_moment[0];
    $actual_moment = $actual_moment[1];
    return path_cache($url).$actual_moment.last_minute($actual_minute).CACHE_EXTENSION;
}
 
 
function last_minute($actual_minute){
  if(!isset($actual_minute)){
    $actual_minute = date('i');
  }
   
  while ($actual_minute%MINUTES_OF_LIFE_OF_THE_CACHE != 0) {
    $actual_minute--;
  }
 
  return $actual_minute;
}

function getID($ipRequest){
    $id = preg_replace('/r=[0-9]*.[0-9]*/', "", $ipRequest);
    $id = preg_replace('/[^0-9_]*/', "", $id);
    $id = preg_replace('/_[0-9]*/', "", $id);

    return empty($id)?-1:$id;
}
 
 
function request($ipRequest){
    $id = getID($ipRequest);
    
    $response = Unirest::get($ipRequest, array( "Accept" => "application/json" ), array( "Accept" => "application/json", "Accept-Charset" => "utf-8" ));
 
    if($response->code==200){
        return $id==-1?$response->body:json_encode(parseData($id, $response->body));
        //return $response->body;
    }else{
        return null;
        //http_response_code($response->code);
    }
}
 
 
function get_last_file($url){
    $path = path_cache($url);
    $ficheros  = scandir($path, SCANDIR_SORT_DESCENDING);
      
    if(count($ficheros)>0){
        return $path.$ficheros[0];
    }else{
        throw new Exception("No cache file found");
    }
}
 
proxy();
     
     
?>